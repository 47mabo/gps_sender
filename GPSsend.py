#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
from gps import *
from time import *
from datetime import datetime
import schedule
import csv
import time
import threading
import requests
import json
import codecs
import glob
gpsd = None
url = "http://437e59b3.ngrok.io/pipeline/v1/things/"
data= 'sudo gpsd /dev/ttyS0 -F /var/run/gpsd.sock'
path = '/home/pi/Desktop/GPS/DataFolder/*.json'
delAll = '/home/pi/Desktop/GPS/DataFolder/*'
os.system(data)
 
#________________________________________________
os.system('clear')
class GpsPoller(threading.Thread):
  def __init__(self):
    threading.Thread.__init__(self)
    global gpsd
    gpsd = gps(mode=WATCH_ENABLE)
    self.current_value = None
    self.running = True
 
  def run(self):
    global gpsd
    while gpsp.running:
      gpsd.next()
     
gpsp = GpsPoller()
 
#________________________________________________
 
def gps_watcher():
    os.system('clear')
    print '..............................'
    print ' GPS watching'
    time.sleep(1)
    gps_timestamp  = gpsd.utc
    gps_satellites = gpsd.satellites
    gps_fix        = json.dumps(gpsd.fix.__dict__)
    log_gps_fix(gps_timestamp,gps_fix,gps_satellites)
 
#________________________________________________
 
def log_gps_fix(times,fix,sat):
    internet_connected=pall_connection()
    if internet_connected:
        send_data(time,fix,sat)
    else:
        save_data(times,fix,sat)
   
#________________________________________________
 
def send_data(times,fix,sat):
  time.sleep(2)
  strdata  = (time,fix,sat)
  dataSend = str(strdata)
  payload  = {'GPS_data':dataSend}
  response = requests.post(url, params=payload)
  print 'send >>>'
#________________________________________________
 
def save_data(times,fix,sat):
    time.sleep(2)
    datafloat      = (times,fix,sat)
    data           = str(datafloat)
    jsonStringData = json.dumps(data)
    with codecs.open('DataFolder/'+times+'.json',"a",encoding='utf8') as fOpen:
        fOpen.write(str(jsonStringData))
    print 'data save on  Pi'
   
#________________________________________________
 
def pall_connection():
    try:
      respond = requests.get(url)
      print respond.status_code
      if respond.status_code == 200:
        print 'True'
        return True
      else:
        print 'server error'
        return False
    except:
      print  'no internet'
      return False
 
#_______________________________________________
def send_if_get_not():
  try:
    files=glob.glob(path)  
    for file in files:
      f=open(file, 'r')
      #print '%s' % f.readlines()  
      line = f.readlines()  
      f.close()
      payload  = {'GPS_data':line}
      response = requests.post(url, params=payload)
      print 'data send>>>>'
    os.system('rm '+delAll)
    time.sleep(2)
    print 'sended'
    #time.sleep(3)
  except:
    print 'data save cant send (no internet)'
    return
schedule.every(10).seconds.do(send_if_get_not)
#_______________________________________________
   
 
if __name__=='__main__':
    gpsp.start()
    while True:
        gps_watcher()
        #time.sleep(1)
        schedule.run_pending()